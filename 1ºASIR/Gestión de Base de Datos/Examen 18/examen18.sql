SET SERVEROUTPUT ON

DECLARE
NUM NUMBER;
BEGIN
NUM:= &iNTRODUCE_UN_NUMERO;
IF NUM=1 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 1 es Lunes.');
ELSIF NUM=2 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 2 es Martes.');
ELSIF NUM=3 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 3 es Miercoles.');
ELSIF NUM=4 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 4 es Jueves.');
ELSIF NUM=5 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 5 es Viernes.');
ELSIF NUM=6 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 6 es Sabado.');
ELSIF NUM=7 THEN
DBMS_OUTPUT.PUT_LINE('El valor introducido 7 es Domingo.');
ELSE DBMS_OUTPUT.PUT_LINE('Este dia de la semana no existe, la semana solo tiene 7 dias');
END IF;
END;
/
----------------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function asteriscos(texto varchar)return varchar is
r INTEGER;
x integer:=1;
p varchar(1);
res varchar(50);
begin
r:=length(texto);
res:=texto;  
while x<=r loop
    p:=substr(texto,x,1); 
    if p!=' ' then
    res:=replace(res,p,'*');
    end if;
     x:=x+1;
end loop;
  
return res;
end asteriscos;
/

declare
texto varchar(50):='&Introduce_la_cadena_de_caracteres';
begin

dbms_output.put_line(asteriscos(texto));
end;
/

-----------------------------------------------------------------

CREATE OR REPLACE PROCEDURE MOSTRARPELICULA
(V_CODIGO IN PELICULA.ID%TYPE,
V_TITULO OUT PELICULA.TITULO%TYPE,
V_GENERO OUT PELICULA.GENERO%TYPE,
V_DIRECTOR OUT PELICULA.DIRECTOR%TYPE ) IS
cursor c_peli is SELECT TITULO, GENERO, DIRECTOR FROM PELICULA WHERE DIRECTOR=V_DIRECTOR;

BEGIN 
 SELECT TITULO, GENERO, DIRECTOR INTO V_TITULO,V_GENERO,V_DIRECTOR FROM PELICULA WHERE ID=V_CODIGO;
   if V_CODIGO is not null then
    DBMS_OUTPUT.PUT_LINE( 
    'TITULO:'|| V_TITULO ||' 
    '||'GENERO:'|| V_GENERO ||' 
    '||'DIRECTOR:'|| V_DIRECTOR);
    end if;
 open c_peli;
 loop
 fetch c_peli into V_TITULO,V_GENERO,V_DIRECTOR;
 exit when c_peli%notfound;
 end loop;
 if c_peli%rowcount>1 then
 dbms_output.put_line('Existen otras peliculas con el mismo director');
 else
 dbms_output.put_line('No existen otras peliculas con el mismo director');
 end if;
 close c_peli;
 
--INTO V_TITULO, V_GENERO, V_DIRECTOR
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('No se encontraron datos.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Ha ocurrido un error.');
END;
/

DECLARE
    V_TITULO PELICULA.TITULO%TYPE;
    V_GENERO PELICULA.GENERO%TYPE;
    V_DIRECTOR PELICULA.DIRECTOR%TYPE;
    V_CODIGO PELICULA.ID%TYPE := &DAME_EL_CODIGO;

BEGIN
    MOSTRARPELICULA(V_CODIGO, V_TITULO, V_GENERO, V_DIRECTOR);


END;
/
-------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ActoresPelicula
(cod pelicula.id%type)
IS
CURSOR ACTORES IS
SELECT A.NOMBRE FROM ACTOR A
JOIN ACTORES_PELICULA AP ON AP.IDACTOR = A.ID
WHERE AP.IDPELICULA=COD;
BEGIN
FOR X IN ACTORES LOOP
DBMS_OUTPUT.PUT_LINE('Nombre del actor: ' || X.NOMBRE);
END LOOP;
EXCEPTION
WHEN NO_DATA_FOUND THEN
DBMS_OUTPUT.PUT_LINE('No hay actores.');
END;
/

DECLARE
    cod pelicula.id%type:=&Introduce;
BEGIN
   ActoresPelicula(cod);

END;
/










