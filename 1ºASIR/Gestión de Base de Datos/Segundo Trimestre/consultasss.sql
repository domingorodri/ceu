select * from futbolistas;
select * from equipos;

select posicion, round(avg(salario),2)
from futbolistas
where posicion is not null
GROUP BY posicion;

select COUNT(*) from futbolistas group by id_equipo;

select COUNT(*) from futbolistas group by id_equipo HAVING id_equipo BETWEEN 1 and 2;
select COUNT(*) from futbolistas group by id_equipo HAVING id_equipo in (1,2);

select * from partidos, equipos;
select * from partidos;
select * from equipos;
select * from partidos p1, partidos p2;
insert into equipos VALUES (4,'domingofclub', 'catuja', 5);
select futbolistas.nombre, equipos.nombre from futbolistas, equipos
where futbolistas.id_equipo(+) = equipos.id;

SELECT FUTBOLISTAS.NOMBRE, EQUIPOS.NOMBRE FROM FUTBOLISTAS, EQUIPOS WHERE FUTBOLISTAS.ID_EQUIPO=EQUIPOS.ID;

select equipos.nombre "nombre equipo",
futbolistas.nombre "nombre futbolista",
futbolistas.posicion "posicion"
from futbolistas, equipos
where futbolistas.id_equipo(+) = equipos.id;

select equipos.nombre,
futbolistas.nombre,
futbolistas.posicion
from futbolistas
cross join equipos;

CREATE TABLE FUTBOLISTAS2(
ID CHAR(6) PRIMARY KEY CHECK( REGEXP_LIKE( ID, 'F[0-9][0-9][0-9]20' )),
NOMBRE VARCHAR(100),
APELLIDOS VARCHAR(300),
FECHA_NACIMIENTO DATE,
POSICION VARCHAR(50) CHECK( POSICION LIKE 'PORTERO' OR POSICION LIKE 'DEFENSA' OR POSICION LIKE 'MEDIOCENTRO' OR POSICION LIKE 'DELANTERO' ),
SALARIO NUMBER(12,2) CHECK( SALARIO > 50000),
ID_EQUIPO INT
);

CREATE TABLE EQUIPOS2(
ID_EQUIPO INT PRIMARY KEY,
NOMBRE VARCHAR(100),
ESTADIO VARCHAR(100),
PRESUPUESTO NUMBER(20,2)
);
alter table futbolistas2
add foreing key (id_equipo)
references equipos2 (id_equipo);