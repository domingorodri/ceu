select distinct INITCAP (posicion),apellidos from futbolistas order by 2;

select '--' || substr(nombre,1,3) from futbolistas where id_equipo = 1;

select substr(nombre,1,3) || '--' from futbolistas where id_equipo = 1;

select replace (nombre, 'LUIS','L') from futbolistas;

select replace (nombre, 'LUIS','L'), length(apellidos) from futbolistas;

select EXTRACT(day from sysdate),
EXTRACT(month from sysdate),
EXTRACT(year from sysdate)
from dual;

select add_months (sysdate,6) from dual;

select ROUND (MONTHS_BETWEEN(SYSDATE,fecha_nacimiento)) from futbolistas;

select last_day(sysdate) from dual;

select NEXT_DAY(sysdate, 'LUNES') from dual;

select sysdate-4 from dual;

select sysdate+1 from dual;

select to_number('1000.450','9999.999') from dual;

select to_number('1000.450�','9999.999L') from dual;

select to_number('-$1000.450','s$9999.999') from dual;

select to_char (sysdate,'dd/mm/yyyy') from dual;

select nvl (to_char(fecha_nacimiento, 'yyyy'),'----') from futbolistas order by 1;
select decode(fecha_nacimiento,null,'----', to_char(fecha_nacimiento,'yyyy')) from futbolistas order by;

alter table futbolistas
add altura int ;
alter table futbolistas
add peso number(4,1);

update futbolistas set altura = 179, peso = 76 where nombre = 'PEDRO LUIS';
update futbolistas set altura = 174, peso = 71.5 where nombre = 'LUIS';
update futbolistas set altura = 169, peso = 66 where nombre = 'JESUS';
update futbolistas set altura = 189, peso = 82 where nombre = 'DIEGO';
update futbolistas set altura = 183, peso = 84 where nombre = 'PABLO';
update futbolistas set altura = 180, peso = 81.5 where nombre = 'ESTEBAN';
update futbolistas set altura = 1, peso = 60 where nombre = 'ENRIQUE';

SELECT NOMBRE, ALTURA
FROM FUTBOLISTAS
WHERE ALTURA = (SELECT MAX(ALTURA) FROM FUTBOLISTAS);






















