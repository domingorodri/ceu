SELECT UPPER(NOMBRE), LOCALIDAD FROM CLIENTES;

SELECT ROUND(AVG(PRECIO),2) FROM PLATOS;

SELECT NOMBRE, TELEFONO FROM CLIENTES 
WHERE LOCALIDAD = 'Gines';

SELECT i.nombre FROM platos p JOIN ingredientes_platos ip 
ON p.id=ip.id_plato JOIN ingredientes i 
ON ip.id_ingrediente=i.id 
WHERE p.nombre='Tarta de queso';

SELECT NOMBRE,CATEGORIA FROM INGREDIENTES 
WHERE NOMBRE LIKE 'Pi%';

SELECT COUNT(*) FROM INGREDIENTES 
WHERE CATEGORIA = 'LACTEO';

SELECT ROUND(p.precio*COUNT(*),2) FROM comandas c
LEFT JOIN platos p ON c.id_plato = p.id WHERE p.id=5;

SELECT AVG(cantidad) FROM ingredientes_platos;

SELECT p.nombre FROM platos p JOIN ingredientes_platos ip ON p.id=ip.id_plato JOIN ingredientes i ON ip.id_ingrediente=i.id WHERE i.nombre='Aceite de oliva virgen extra';

SELECT COUNT(*) FROM comandas WHERE estado='SERVIDO'
AND fecha='02-03-2021';

SELECT p.nombre FROM COMANDAS com 
JOIN PLATOS p ON com.id_plato=p.id WHERE estado='DEVUELTO';

SELECT SYSDATE - FECHA 
FROM COMANDAS ORDER BY FECHA DESC FOR UPDATE 1;