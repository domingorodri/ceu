DROP TABLE PERSONAS CASCADE CONSTRAINTS;

CREATE TABLE PERSONAS(
    DNI CHAR(9) PRIMARY KEY,
    NOMBRE VARCHAR(50)
    
    );

alter table PERSONAS
add TALLA VARCHAR(3);

ALTER TABLE PERSONAS
ADD CONSTRAINT TALLA CHECK (TALLA IN ('S','M','L','XL','XXL'));

ALTER TABLE PERSONAS
ADD CONSTRAINTS DNI CHECK (REGEXP_LIKE(DNI, '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Z]'));

INSERT INTO PERSONAS VALUES ('12345678A','LUIS','L');
INSERT INTO PERSONAS VALUES ('12345658A','MARTIN','L');
INSERT INTO PERSONAS VALUES ('92245678A','LUISA','XL');

/*CAMBIAR DATO DE UN REGISTRO YA METIDO*/

UPDATE PERSONAS set TALLA='XXL' WHERE DNI='92245678A';