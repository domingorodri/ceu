--Ejercicio 1
set serveroutput on; 
DECLARE
horasSemanales constant int := &introducir_horas_semanales;
costeHora number(5,2):=&Introduce_el_coste_hora;
user varchar(30) := &Introduzca_nombre_de_usuario;
res NUMBER;
fecha date;
BEGIN
costeHora := costeHora * 1.5;
res := costeHora * horasSemanales;
case
when horasSemanales <= 0 then
DBMS_OUTPUT.PUT_lINE('Las horas semanales debe ser mayor que 0');
when costeHora <= 0 then
DBMS_OUTPUT.PUT_LINE('EL coste hora debe ser mayor a 0');
when user is null then
dbms_output.put_line('No es valido');
when res > 100 then
select sysdate into fecha from dual;
dbms_output.put_line(user||' gana semanalmente '||res||' euros '||' (fecha: '|| fecha || ')');
else
dbms_output.put_line('no es mayor que 100');
end case;
END;
/

--Ejercicio 2

declare
x number:=0;
y number:=0;
m number:=0;
begin
for i in 1..100000 loop
 m:=i;
 for j in 2..i-1 loop
 y:=y+1;
 if mod (i, j) <> 0 then 
   x:=x+1;
   end if;
   end loop;
 if x=y then
       dbms_output.put_line ('n�mero primo es ' || m);
   end  if ;
 x:=0;
 y:=0; 
end loop;
end;
/

--Ejercicio 3
DECLARE
DOS INT := 2;
BEGIN
FOR DOS IN 1..40 LOOP
IF MOD(DOS) = 1
DBMS_OUTPUT.PUT_LINE(RESULTADO)
END IF;
RESULTADO:=DOS*4;
END LOOP;
END;
/

