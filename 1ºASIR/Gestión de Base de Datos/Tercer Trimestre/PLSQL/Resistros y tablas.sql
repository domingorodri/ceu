set serveroutput on; 
--Ejercicio 1 --registro
declare
type tpersona is record(
codigo number(2),
nombre varchar(100),
edad int
);
var1 tpersona;
begin
var1.codigo := 4;
var1.nombre := 'Manuel';
var1.edad := 22;
dbms_output.put_line('C�digo: ' || var1.codigo || ', nombre: ' || var1.nombre || ', edad: ' || var1.edad);
end;
/
--atributo %type --implicito
declare
type TEMPLEADO IS RECORD(
codigo EMP.EMPNO%TYPE,
nombre EMP.ENAME%type,
puesto emp.job%type
);
v_var1 TEMPLEADO;
begin
select empno, ename, job into v_var1
from emp
where empno = 7900;
dbms_output.put_line('El c�digo es ' || v_var1.codigo || ' y adem�s, se llama ' || v_var1.nombre || ' y por �ltimo, trabaja como ' || v_var1.puesto );
end;
/

--atributo %Rowtype
declare 
    v_var1  emp%rowtype;
begin
    select * into v_var1 from emp where empno = 7900;
    dbms_output.put_line (v_var1.ename);
end;

/

--tablas, sintaxis

declare
    type tablaPrueba is table of int index by binary_integer;
    v_var2 tablaPrueba;
begin
    v_var2(4) := 90;
    dbms_output.put_line(v_var2(4));
    
 end;
 /

--Ejercicio 1
declare
    type animales is table of varchar(20) index by binary_integer;
    variable1 animales;
    
    begin
        variable1(1) := 'PERRO';
        DBMS_OUTPUT.put_line(variable1(1));
    end;
    /
    
--Ejercicio 2

DECLARE
TYPE NUMEROS IS TABLE OF INT INDEX BY BINARY_INTEGER;
VN NUMEROS;
BEGIN
FOR X in 1..10 LOOP
    vn(x):=x;
    
    DBMS_OUTPUT.PUT_LINE(VN(X));
  END LOOP;
END;
/
DECLARE

TYPE NUMEROS IS TABLE OF INT INDEX BY BINARY_INTEGER;
TYPE EMPLEADOS IS RECORD(
CODIGO NUMBER(2),
NOMBRE VARCHAR2(20)
);
TYPE TEMP IS TABLE OF EMPLEADOS INDEX BY BINARY_INTEGER;
V_EMP TEMP;

BEGIN
V_EMP(1).CODIGO := 1;
V_EMP(1).NOMBRE := 'LUIS';
DBMS_OUTPUT.PUT_LINE('Nombre: ' || v_emp(1).nombre || ' | C�digo: ' || v_emp(1).codigo);
END;
/
--EJERCICIO 3

DECLARE
TYPE PERSONAS IS RECORD(
NOMBRE VARCHAR(20),
APELLIDO1 VARCHAR2(20),
APELLIDO2 VARCHAR2(20)
);
TYPE TEMP IS TABLE OF PERSONAS INDEX BY BINARY_INTEGER;
V_EMP TEMP;

BEGIN
V_EMP(1).NOMBRE := 'DOMINGO';
V_EMP(1).APELLIDO1 := 'RODRIGUEZ';
V_EMP(1).APELLIDO2 := 'FIZ';

DBMS_OUTPUT.PUT_LINE('Nombre: ' || V_EMP(1).NOMBRE ||' '|| V_EMP(1).APELLIDO1 ||' '|| v_emp(1).APELLIDO2);
END;
/

--EJERCICIO 4

DECLARE
TYPE NUMEROS IS TABLE OF INT INDEX BY BINARY_INTEGER;
VN NUMEROS;
BEGIN
FOR X in 1..10 LOOP
    vn(x):=x;
    
    DBMS_OUTPUT.PUT_LINE(VN(X));
  END LOOP;
    DBMS_OUTPUT.PUT_LINE('NUMERO DE ELEMENTOS' ||' '|| VN.COUNT);
    DBMS_OUTPUT.PUT_LINE('NUMERO DE ELEMENTOS' ||' '|| VN.FIRST);
    DBMS_OUTPUT.PUT_LINE('NUMERO DE ELEMENTOS' ||' '|| VN.LAST);
   IF VN.EXISTS(10) THEN
dbms_output.put_line('Existe');
ELSE
dbms_output.put_line('No existe');
END IF;
END;
/

--JERCICIOS DE REPASO

DECLARE
TYPE PAIS IS RECORD (
CO_PAIS NUMBER,
DESCRIPCION VARCHAR2(50),
CONTINENTE VARCHAR2(20));
TYPE PAISES IS TABLE OF PAIS INDEX BY BINARY_INTEGER;
tPAISES PAISES;
BEGIN
tPAISES(1).CO_PAIS := 27;
tPAISES(1).DESCRIPCION := 'ITALIA';
tPAISES(1).CONTINENTE := 'EUROPA';
DBMS_OUTPUT.PUT_LINE(tPAISES(1).CO_PAIS ||' '|| tPAISES(1).DESCRIPCION ||' '|| tPAISES(1).CONTINENTE);
END;
/






