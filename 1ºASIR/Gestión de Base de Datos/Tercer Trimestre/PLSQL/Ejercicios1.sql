set serveroutput on
--Ejercicio 1
declare
num1 int;
num2 int;
begin

num1:=&Introduce_el_primer_numero;
num2:=&Introduce_el_segundo_numero;

if num1>=num2 then
dbms_output.put_line('Son divisibles');
else
dbms_output.put_line('No son divisibles');
end if;

end;
/




--Ejercicio 2
set serveroutput on
declare
variable char(20);
begin
select to_char(sysdate,'DAY') into variable
from dual;
dbms_output.put_line(variable);
end;
/

--Ejercicio 3
declare
num1 int; --Declaracion de datos
num2 int;
num3 int;
numero_negativo exception; --Excepcion de numeros negativos
begin
num1:=&Introduce_el_primer_numero; --Introducir los datos
num2:=&Introduce_el_segundo_numero;
num3:=&Introduce_el_tercero_numero;

if num1 < 0 then --Excepcion num1
    raise numero_negativo;
  end if;
  
  if num2 < 0 then ----Excepcion num2
    raise numero_negativo;
  end if;
  
  if num3 < 0 then ----Excepcion num3
    raise numero_negativo;
  end if;
  
dbms_output.put_line(num1+num2+num3); --Suma de los 3 numeros
exception --Excepcion de los 3 numeros cuando alguno sea menor a 0
when numero_negativo then
    DBMS_OUTPUT.PUT_LINE('El numero no puede ser negativo');
end;
/

--Ejercicio 4
set serveroutput on
DECLARE
salario number;
BEGIN
salario := &Introduce_el_salario;
CASE
WHEN salario <= 0 THEN
dbms_output.put_line('Salario no valido');
WHEN salario <20000 THEN
dbms_output.put_line('Tu salario es bajo');
WHEN salario >20000 and salario < 40000 THEN
dbms_output.put_line('Tu salario esta bien');
WHEN salario >40000 THEN
dbms_output.put_line('Tu salario esta muy bien');
ELSE
dbms_output.put_line('El valor introducido es incorrecto');
END CASE;
END;
/










