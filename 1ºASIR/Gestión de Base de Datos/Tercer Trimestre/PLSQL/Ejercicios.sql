
set serveroutput on; --SALIDA POR PANTALLA
DECLARE
RESULTADO INT;
begin
RESULTADO := 10; --VARIABLE
DBMS_OUTPUT.PUT_LINE('test'|| RESULTADO);
END;
/

--BASE DE UN TRIANGULO
set serveroutput on; --SALIDA POR PANTALLA
DECLARE
BASE INT;
ALTURA INT;
begin
BASE := &DAME_LA_BASE; --VARIABLE
ALTURA := &DAME_LA_ALTURA;
DBMS_OUTPUT.PUT_LINE(BASE*ALTURA/2);
END;
/

set serveroutput on; --SALIDA POR PANTALLA
DECLARE
NOMBRE VARCHAR(50);
APELLIDOS VARCHAR(50);
begin
NOMBRE := &DAME_NOMBRE; --VARIABLE
APELLIDOS := &DAME_APELLIDOS;
DBMS_OUTPUT.PUT_LINE('HOLA'|| NOMBRE || ' ' || APELLIDOS );
END;
/

set serveroutput on; --SALIDA POR PANTALLA
DECLARE
NUM1 INT;
NUM2 INT;
begin
NUM1 := &DAME_NUM1; --VARIABLE
NUM2 := &DAME_NUM2;
DBMS_OUTPUT.PUT_LINE(NUM1+NUM2);
DBMS_OUTPUT.PUT_LINE(NUM1-NUM2);
DBMS_OUTPUT.PUT_LINE(NUM1*NUM2);
DBMS_OUTPUT.PUT_LINE(NUM1/NUM2);
END;
/

declare
num1 number;
num2 number;
begin

num1:=&Introduce_el_primer_numero;
num2:=&Introduce_el_segundo_numero;

if num1>num2 then
dbms_output.put_line(num1-num2);
else
dbms_output.put_line( num1 ||' es menor que '||num2);
end if;

end;
/


declare
num1 INT;
num2 INT;
begin

num1:=&GOLES_EQUIPO_DE_CASA;
num2:=&GOLES_EQUIPO_DE_FUERA;
CASE
WHEN num1>num2 then
dbms_output.put_line('EL EQUIPO DE CASA HA GANADO');
WHEN num1<num2 then
dbms_output.put_line('EL EQUIPO DE FUERA HA GANADO');
WHEN num1=num2 then
dbms_output.put_line('EL RESULTADO HA SIDO EMPATE');
else
dbms_output.put_line('NO HA INTRODUCIDO VALORES CORRECTOS');
end CASE;
end;
/

declare
variable1 int:=0;
begin
loop
dbms_output.put_line(variable1);
variable1:=variable1 + 1;
exit when variable1 >80;
end loop;
end;
/

declare
 x number(8) := 1;
begin

while (x<=10)
loop
dbms_output.put_line(x);
x := x+1;
end loop;
end;
/


set serveroutput on;
declare
x int:=0;
begin
while x<= 20 loop
dbms_output.put_line(x);
x := x+1;
end loop;
end;
/

begin
for i in reverse 12..100 loop
    dbms_output.put_line(i);
    end loop;
end;
/
begin
for i in 12..100 loop
    dbms_output.put_line(i);
    end loop;
end;
/
DECLARE
mult INT;
BEGIN
FOR i IN 1 .. 10 LOOP
FOR j IN 1..10 LOOP
mult := i * j;
dbms_output.put_line(i || ' * ' || j || ' = ' || mult);
END LOOP;
END LOOP;
END;
/

declare --Ejercicio 1
num1 int;
begin
num1:= &dame_num1;
if num1>0 then
DBMS_OUTPUT.PUT_lINE('Es mayor que cero');
end if;
end;
/

declare --Ejercicio 2
num1 int;
begin
num1:= &dame_num1;
if num1>5 then
DBMS_OUTPUT.PUT_lINE('Es mayor que 5');
else
DBMS_OUTPUT.PUT_lINE('Es menor que 5');
end if;
end;
/

set serveroutput on --Ejercicio 3
declare
num1 int;
begin
num1:= &n�mero;
if num1>0 then
DBMS_OUTPUT.PUT_lINE('Es positivo');
end if;
if num1<0 then
DBMS_OUTPUT.PUT_lINE('Es negativo');
end if;
if num1=0 then
DBMS_OUTPUT.PUT_lINE('Es cero');
end if;
end;
/

set serveroutput on --Ejercicio 4
declare
num1 int;
begin
num1:= 10;
if mod(num1,2)=0 then
DBMS_OUTPUT.PUT_lINE(num1||' es par');
else
DBMS_OUTPUT.PUT_lINE(num1||' es impar');
end if;
end;
/

--Ejercicio 5
declare
palabra varchar(50);
begin
palabra:= &Dame_la_palabra;
if length (palabra) <= 5 then
DBMS_OUTPUT.PUT_lINE('Es corta');
end if;
if length (palabra) >5 and length(palabra)<15 then
DBMS_OUTPUT.PUT_lINE('Es normal');
end if;
if length (palabra) >= 15 then
DBMS_OUTPUT.PUT_lINE('Es larga');
end if;
end;
/

--Ejercicio 6
declare
palabra varchar(50);
begin
palabra := '&Dame_palabra';
CASE
WHEN length (palabra) <= 5 then
DBMS_OUTPUT.PUT_lINE('Es corta');
WHEN length (palabra) >5 and length (palabra) <15  then
DBMS_OUTPUT.PUT_lINE('Es normal');
WHEN length (palabra) >15 then
DBMS_OUTPUT.PUT_lINE('Es larga');
end CASE;
end;
/


--Ejercicio 7
set serveroutput on
DECLARE
nota number(10,2);
BEGIN
nota := &Introduce_tu_nota;
CASE
WHEN nota >0 and nota <5 THEN
dbms_output.put_line('Insuficiente');
WHEN nota >=5 and nota <=6 THEN
dbms_output.put_line('Aprobado');
WHEN nota >=6 and nota <=7 THEN
dbms_output.put_line('Bien');
WHEN nota >=7 and nota <=9 THEN
dbms_output.put_line('Notable');
WHEN nota >=9 and nota <=10 THEN
dbms_output.put_line('Sobresaliente');
ELSE
dbms_output.put_line('El valor introducido es incorrecto');
END CASE;
END;
/

--Ejercicio 8

declare
x int := 0;
begin
for x in 1 .. 10 loop
dbms_output.put_line(x);
end loop;
end;
/


--Ejercicio 9
declare
x int := 1;
begin
while x<=10 loop
exit when x > 10;
dbms_output.put_line(x);
x:=x+1;
end loop;
end;
/


--Ejercicio 10
declare
x int := 1;
begin
loop
dbms_output.put_line(x);
x := x+1;
exit when x >10;
end loop;
end;
/

--Ejercicio 11
declare
x int := 1;
begin
loop
dbms_output.put_line(x ||',');
x := x+1;
exit when x >10;
end loop;
end;
/


--Ejercicio 12
BEGIN
    FOR i IN REVERSE 1..10 LOOP
        dbms_output.put_line(i);
    END LOOP;
END;
/

--Ejercicio 13

declare
x int := 0;
begin
while (x<=100) loop
if x mod 2 = 0 then
dbms_output.put_line(x);
end if;
x:=x+1;
end loop;
end;
/


declare
x int := 0;
begin
loop
DBMS_OUTPUT.PUT_LINE (x);
x:= x+1;
exit when x > 20;
end loop;
end;
/

declare
x int := 1;
begin
while (x <= 200) loop
exit when x > 1000;
DBMS_OUTPUT.PUT_LINE(x);
x:=x+1;
end loop;
end;
/

declare
x int := 1;
begin
for x in 1..100 loop
DBMS_OUTPUT.PUT_LINE(x);
end loop;
end;
/


































