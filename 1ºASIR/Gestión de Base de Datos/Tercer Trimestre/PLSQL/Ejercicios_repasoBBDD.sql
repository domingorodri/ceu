set serveroutput on;

--Ejercicio 1
declare
x int;
begin
x := &Dame_un_numero;
IF x > 0 then
dbms_output.put_line('Es mayor que 0');
end if;
end;
/

--Ejercicio 2

declare
x int;
begin
x := &Dame_un_numero;
IF x >= 5 then
dbms_output.put_line('Es mayor que 5');
else 
dbms_output.put_line('Es menor que 5');
end if;
end;
/

--Ejercicio 3

declare
x int;
begin
x := &Dame_un_numero;
IF x = 0 then
dbms_output.put_line('Es 0');
elsif (x < 0) then
dbms_output.put_line('Es negativo');
else 
dbms_output.put_line('Es positivo');
end if;
end;
/


--Ejercicio 4

begin
if 30 mod 2 = 0 then
dbms_output.put_line('Es par');
else 
dbms_output.put_line('Es inpar');
end if;
end;
/


--Ejercicio 5

declare
palabra varchar(50);
begin
palabra := &Dame_una_palabra;
IF length(palabra) <= 5 then
dbms_output.put_line('Es corta');
elsif length(palabra) < 15 then
dbms_output.put_line('Es normal');
else 
dbms_output.put_line('Es larga');
end if;
end;
/


--Ejercicio 6

DECLARE
    my_string VARCHAR(100) := '&cadena';
BEGIN
    CASE
    WHEN LENGTH(my_string) <= 5 THEN
        dbms_output.put_line('Es corta.');
    WHEN LENGTH(my_string) < 15 THEN
        dbms_output.put_line('Es normal.');
    ELSE
        dbms_output.put_line('Es larga.');
    END CASE;
END;
/


--Ejercicio 8
begin
for i in 1..10 loop
dbms_output.put_line(i);
end loop;
end;
/

--Ejercicio 9
declare
i int:= 0;
begin
while i <= 10 loop
dbms_output.put_line(i);
i := i+1;
end loop;
end;
/












