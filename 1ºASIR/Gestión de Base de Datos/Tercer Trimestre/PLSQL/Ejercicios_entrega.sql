set serveroutput on; 


--Ejercicio 1
declare 
    actividad emp%rowtype;
    type numeros is table of int index by binary_integer;
    a numeros;
    x int := 1;
begin
    a(1) := 7839;
    a(2) := 7698;
    a(3) := 7782;
    
    while x <= a.count() loop
        select * into actividad from emp where empno = a(x);
        DBMS_OUTPUT.put_line(actividad.ename );
        x := (x + 1);
    end loop;
    /*for x in 1 .. a.count() loop   ---Tambien lo he probado con for
        select * into actividad from emp where empno = a(x);
        DBMS_OUTPUT.put_line(actividad.ename );
    end loop;*/
end;
/


--Ejercicio 2
declare
type persona is record(
codigo number(4),
nombre varchar2(10),
trabajo varchar2(9)
);
type personas is table of persona index by binary_integer;
ps1 personas;

begin
    select empno, ename, job into ps1(1) from emp where empno = 7839;
    DBMS_OUTPUT.put_line(ps1(1).codigo ||' '|| ps1(1).nombre ||' '|| ps1(1).trabajo);
end;
/




