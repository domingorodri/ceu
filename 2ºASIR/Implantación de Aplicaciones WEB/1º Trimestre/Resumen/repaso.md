# Implantación de Aplicaciones WEB

## Resumen

### Conceptos generales de la arquitectura de aplicaciones web

- Concepto

  Las aplicaciones web utilizan lo que se conoce como clientes livianos (light clients) los cuales no ejecutan demasiadas labores de procesamiento para la ejecución de la aplicación misma.

<<<<<<< HEAD
<br>

- ¿QUÉ PASA CON LAS APLICACIONES DE CONSOLA O MODO TEXTO?
  

  Con aplicaciones de consola nos referimos a las construidas en plataformas tipo Cobol, RPG para AS400 y FoxPro, entre otras. El concepto de las aplicaciones de consola es parecido al de una aplicación web con una arquitectura del tipo cliente-servidor en la cual el cliente también se puede considerar liviano. Aunque existen algunas diferencias como son: n Protocolos de comunicación propios y no estándar, como ocurre en la Web con el protocolo HTTP y el concepto de URL. Formatos de intercambio propios y no estándar, como ocurre en la Web con el formato HTML o XML. En el lado del cliente hay restricciones con las vistas, ya que es necesario instalar API específicas que no son estándar, portables o extensibles. En la Web solo se debe instalar un navegador para acceder a la aplicación.

<br>

=======
  Con aplicaciones de consola nos referimos a las construidas en plataformas tipo Cobol, RPG para AS400 y FoxPro, entre otras. El concepto de las aplicaciones de consola es parecido al de una aplicación web con una arquitectura del tipo cliente-servidor en la cual el cliente también se puede considerar liviano. Aunque existen algunas diferencias como son: Protocolos de comunicación propios y no estándar, como ocurre en la Web con el protocolo HTTP y el concepto de URL. Formatos de intercambio propios y no estándar, como ocurre en la Web con el formato HTML o XML. En el lado del cliente hay restricciones con las vistas, ya que es necesario instalar API específicas que no son estándar, portables o extensibles. En la Web solo se debe instalar un navegador para acceder a la aplicación.
>>>>>>> e684dd35ce745c594e6e7b5c7b0e139ff1ee097a
- VENTAJAS DEL SOFTWARE WEB

  - No requiere instalar software especial (en los clientes).
  - Bajo coste en actualizar los equipos con una nueva versión
  - Acceso a la última y mejor versión.
  - Información centralizada.
  - Seguridad y copias de seguridad.
  - Movilidad.
  - Reducción de costes en los puestos cliente (mayor longevidad).
  <br>
  
- Características de la arquitectura C/S

  - Es el que inicia solicitudes o peticiones. Tiene, por tanto, un papel activo en la comunicación (dispositivo maestro o amo).
  - Espera y recibe las respuestas del servidor. 4 Por lo general, puede conectarse a varios servidores a la vez.
  - Normalmente, interactúa directamente con los usuarios finales mediante una interfaz gráfica de usuario.
  - Al contratar un servicio de red, se debe de tener encuenta  la velocidad de conexión que se le otorga al cliente y el tipo de cable que utiliza.
  
  <br>

  **Al receptor de la solicitud enviada por el cliente se conoce como servidor. Sus características son:**

  - Al iniciarse espera a que le lleguen las solicitudes de los clientes. Desempeñan entonces un papel pasivo en la comunicación (dispositivo esclavo).
  - Tras la recepción de una solicitud, la procesan y luego envían la respuesta al cliente.
  - Por lo general, aceptan conexiones desde un gran número de clientes (en ciertos casos el número máximo de peticiones puede estar limitado).
  - No es frecuente que interactúen directamente con los usuarios finales.
  
- Ventajas:

  - Centralización del control
  - Escalabilidad
  - Fácil mantenimiento
  - Tecnologías
  
- Desventajas

  - Congestión del tráfico.
  - El paradigma de C/S clasico no tiene robustez de una red P2P.
  - Se necesita un hardware específico.
  - El cliente no dispone de los recursos que puedan existir en el servidor.

  ### Arquitectura de 3 Niveles


  - Cliente.
  - El servidor de aplicaciones.
  - El servidor de datos.

    ![Foto](Fotos/1.jpg)

    - Un mayor grado de flexibilidad.
    - Mayor seguridad, ya que la seguridad se puede definir independientemente para cada servicio y en cada nivel.
    - Mejor rendimiento, ya que las tareas se comporten entre servidores.

  ### Protocolo HTTP

  El protocolo HTTP (Hiper Text Transfer Protocol) (Protocolo de transferencia de hipertexto), es el protocolo mas utilizado de internet (Escritas en HTML).

  - El navegador realiza una solicitud HTTP.
  - El servidor procesa la solicitud y despues envia una respuesta HTTP.

  <br>

  ### Protocolo HTTPS

  El protocolo seguro de Transferencia de hipertexto (HTTPS, Hiper Text Transfer Protocol Secure) es la versión segura del protocolo HTTP. La diferencia es que HTTPS permite realizar transacciones de forma segura. Por lo tanto, podremos desarrollar actividades de tipo e-commerce, acceso a cuentas bancarias on line, tramites con la administración pública, etc.

  <br>

  ### Protocolo FTP

  El protocolo FTP (File Transfer Protocol, Protocolo de transferencia de archivos) es, como su propio nombre indica, un protocolo para transferir archivos.

  Se remonta a 1971, cunado se desarrolló un sistema de transferencia de archivos.

  - Permitir que equipos remotos puedan compartir archivos.
  - Permitir la independencia entre los sistemas de archivo del equipo del cliente y del equipo del servidor.
  - Permitir una transferencia de datos eficaz.

  ### Xampp

  El fichero de configuración de Apache es el archivo httpd.conf , un archivo de texto sin formato.
  Se encuentra en C:\xampp\apache\conf\

  ### Aplicaciones WEB

  - ¿Que es un servidor web?

    - Es una pieza de software que responde a las peticiones de los navegadores y entrega la página para el navegador a traves de internet. Cuando se llama a una pagina web mediante una URL (Uniform Resource Locator) se establece una comunicacion entre el navegador del cliente y el servidor utilizando protocolos establecidos.
  - Requerimientos

    - ![Foto](Fotos/2.jpg)

    1) El navegador se comunica con un servidor de nombres (DNS) para obtener la dirección IP que corresponde al nombre de la máquina
    2) Con esta IP se conecta a la máquina servidor.
    3) E l navegador crea una conexión con la dirección IP del servidor en el puerto 80, que es el puerto utilizado por defecto en los servidores web.
    4) Una vez el cliente y el servidor están conectados, se entienden utilizando el protocolo http para que el cliente indique qué recursos necesita del servidor.
    5) El servidor enviará estos recursos al cliente a través de la conexión establecida.

  <br>

  - HTTP: Comunicación Cliente-Servidor

    ![Foto](Fotos/3.jpg)

  <br>

  - Metodos GET y POST

    - ## Métodos de peticion HTTP: GET

      Con el metodo GET, los datos que se envian al servidor se escriben en la misma direccion URL. En la ventana del navegador, lo encontrarás así.

      Ejemplo:

      www.ejemplo.com/registrarse.php?n ombre=pedro&amp;amp;apellido=p erez&amp;amp;edad=55&amp;amp; genero=hombre

      ### **Ventajas:**

      Los parametros URL se pueden guardar junto a la dirección URL como marcador.

      Se pueden volver a acceder a la página a través del historial del navegador.

      ### **Desventajas:**

      Débil protección de los datos

      Capacidad es limitada
      <br>
    - ## Métodos de petición HTTP: Post

      El metodó POST introduce los parámetros en la solicitud HTTP para el servidor. Por ello, no quedan visibles para el usuario. Además, la capacidad del metodo POST es ilimitada.

      ### **Ventajas:**

      Mucha discrepcion al rellenar formularios de usuarios y contraseñas.

      Se pueden enviar textos cortos, fotos y videos.

      ### **Desventajas:**

      Falsos duplicados o riesgo en los formularios web

      Lo datos transferidos con el metodo POST no pueden guardarse junntos al URL como marcador.

      ![Foto](Fotos/4.jpg)


      <br>

      Como podemos ver el servidor WEB mas utilizado es APACHE, ya que esta muy estandarizado.
      ![Foto](Fotos/5.jpg)

      <br>
       Estos son los lenguajes para crear aplicaciones web

      ![Foto](Fotos/6.jpg)

      <br>

      Estos son los Frameworks.
      ![Foto](Fotos/7.jpg)
