--Para crear usuarios
CREATE USER username IDENTIFIED BY password;

--Asignarle permisos
GRANT INSERT, UPDATE ON table_name TO username;

--Asignación de permisos de administración
GRANT DBA TO nombre_usuario;

--Para copiar una tabla
create table dept_copia as select * from dept;

--Eliminar tabla
drop table emp_copia;

--Tablespace
Alter session set "_ORACLE_SCRIPT"= true;

CREATE TABLESPACE TS_USU3 DATAFILE 'C:\ABG\TS\TS_USU3.DBF' SIZE 10M;

CREATE USER USU3 IDENTIFIED BY USU3 DEFAULT TABLESPACE TS_USU3;

CREATE TABLESPACE TS_USU4 DATAFILE 'C:\ABG\TS\TS_USU4.DBF' SIZE 10M;

CREATE USER USU4 IDENTIFIED BY USU4 DEFAULT TABLESPACE TS_USU4;

CREATE TABLE USU3.T_USUARIO3 (A VARCHAR2(20));
CREATE TABLE USU4.T_USUARIO4 (A VARCHAR2(20));


SELECT * FROM all_tables A
WHERE A.OWNER LIKE 'USU%';


------------------------------------------------------------------------------------------------------
-- Haz un trigger que solo permita a los vendedores tener comisiones.
CREATE OR REPLACE TRIGGER NO_COMM_NO_SALES
BEFORE --AFTER
INSERT OR UPDATE OF COMM OR UPDATE OF JOB ON emp_copia --ACCIONES
FOR EACH ROW
BEGIN
        IF UPPER(:NEW.JOB) <> 'SALESMAN' AND :NEW.COMM IS NOT NULL THEN
                raise_application_error(-20669,'NO SE PERMITEN COMISION A ALGUIEN QUE NO SEA VENDEDOR');
        END IF;
END; 

-------------------------------------------------------------------------------------------------------------------
-- Haz un trigger que controle si los sueldos están en los siguientes rangos:
-- CLERK: 800 – 1100
-- ANALYST: 1200 – 1600
-- MANAGER:1800 – 2000

CREATE OR REPLACE TRIGGER sal_range
    BEFORE INSERT OR UPDATE
    ON EMP_COPIA
    FOR EACH ROW
BEGIN
    IF UPPER(:NEW.JOB) = 'CLERK' AND :NEW.SAL NOT BETWEEN 800 AND 1100

 

    THEN
        raise_application_error(-20669, 'Clerk salary must be between 800-1100');
    ELSIF UPPER(:NEW.JOB) = 'ANALYST' AND :NEW.SAL NOT BETWEEN 1200 AND 1600
    THEN
        raise_application_error(-20669, 'Analyst salary must be between 1200-1600');
    ELSIF UPPER(:NEW.JOB) = 'MANAGER' AND :NEW.SAL NOT BETWEEN 1800 AND 2000
    THEN
        raise_application_error(-20669, 'Manager salary must be between 1800-2000');
    END IF;
END;
/
UPDATE EMP_COPIA SET SAL=5 WHERE EMPNO=7788;

--------------------------------------------------------------------------------------------------------------------------
-- Haz un trigger que impida al usuario MANOLO que cambie el sueldo de los empleados que trabajan en DALLAS.
CREATE OR REPLACE TRIGGER SUELDO_DALLAS
BEFORE --AFTER
UPDATE OF SAL ON EMP --ACCIONES
FOR EACH ROW -- PUEDE QUE NO ESTE---PERO SI ESTA EXISTEN LAS VARIABLES :NEW Y :OLD
DECLARE
V_LOC EMP.LOC%TYPE;
BEGIN

    IF USER = 'MANOLO' THEN
    SELECT UPPER(LOC) INTO V_LOC
    FROM DEPT
    WHERE DEPTNO = (:NEW.DEPTNO);
END; 

--------------------------------------------------------------------------------------------------------------------------
--Impedir que se pidan productos que no tiene stock suficiente.
CREATE OR REPLACE TRIGGER NO_PEDIR_SIN_STOCK
BEFORE
INSERT OR UPDATE OF CANTIDAD OR UPDATE OF CODIGOPRODUCTO ON DETALLEPEDIDOS
FOR EACH ROW
DECLARE
    V_CANT_STOCK productos.cantidadenstock%TYPE;
BEGIN
    SELECT cantidadenstock INTO V_CANT_STOCK
    FROM PRODUCTOS
    WHERE codigoproducto = :NEW.CODIGOPRODUCTO;
    
    IF V_CANT_STOCK < :NEW.CANTIDAD THEN
        raise_application_error(-20669,'NO HAY STOCK SUFICIENTE');  
    END IF;
END;

-----------------------------------------------------------------------------------------------------------------------
-- Poner comentario con retraso en pedidos
CREATE OR REPLACE TRIGGER PONER_COMENTARIO
BEFORE 
INSERT OR UPDATE OF FECHAESPERADA OR UPDATE OF FECHAENTREGA ON PEDIDOS
FOR EACH ROW
BEGIN
    IF :NEW.FECHAENTREGA > :NEW.FECHAESPERADA THEN
        :NEW.COMENTARIOS := 'PEDIDO CON RETRASO';
    END IF;
    
END;

--------------------------------------------------------------------------------------------------------------------
-- si se cambia de puesto que se le suba un 10%-- 1.10
CREATE OR REPLACE TRIGGER TR_CAMBIO_PUESTO2
BEFORE
UPDATE OF DEPTNO ON EMP
FOR EACH ROW
BEGIN
    IF :NEW.DEPTNO <> :OLD.DEPTNO THEN
        :NEW.SAL := :OLD.SAL *1.10;
    END IF;

 

END;

-----------------------------------------------------------------------------------------------------------------------------------
-- IMPEDIR QUE SE CAMBIE QUE SE CAMBIA UN NOMBRE
CREATE OR REPLACE TRIGGER CAMBIAR_NOMBRE_CON_NUMERO
BEFORE
UPDATE OF NOMBRE ON EMPLEADOS
FOR EACH ROW
BEGIN
/*
    FOR K IN 0..9
    LOOP
        IF INSTR(:NEW.NOMBRE,K,1)> 0 THEN
            raise_application_error(-20669,'EL NOMBRE CONTIENE UN NUMERO');  
        END IF;
    END LOOP;
 */   
    FOR h in 1..length(:new.nombre) 
    loop
        for j in 0..9
        loop
            if substr(:new.nombre,h,1)  = to_char(j) then
                raise_application_error(-20669,'EL NOMBRE CONTIENE UN NUMERO');
            end if;
        end loop;
    end loop;
END;

---------------------------------------------------------------------------------------------------------------------------------------------------
ALTER SESSION SET "_ORACLE_SCRIPT" = TRUE;
set serveroutput on;

CREATE TABLESPACE TBSP_COMERCIAL
    DATAFILE 'C:\ABD\TS\tbsp_comercial.dbf'
    SIZE 1M;
CREATE TABLESPACE TBSP_FABRICA
    DATAFILE 'C:\ABD\TS\tbsp_fabrica.dbf'
    SIZE 1M;

CREATE USER COMERCIAL IDENTIFIED BY COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
CREATE USER FABRICA IDENTIFIED BY FABRICA
    DEFAULT TABLESPACE TBSP_FABRICA;

DROP TABLE COMERCIAL.clientes;
CREATE TABLE COMERCIAL.clientes(
    codigo INT,
    nombre VARCHAR(50),
    fecha_registro DATE);

CREATE TABLESPACE TBSP_PEDIDOS
    DATAFILE 'C:\ABD\TS\tbsp_pedidos.dbf'
    SIZE 1M
    AUTOEXTEND ON MAXSIZE 10M;

DROP TABLE COMERCIAL.pedidos;
CREATE TABLE COMERCIAL.pedidos(
    num_pedido INT,
    cliente_codigo INT,
    articulo_codigo VARCHAR2(20),
    precio_total NUMBER,
    fecha_pedido DATE) TABLESPACE TBSP_PEDIDOS;

DROP TABLE FABRICA.articulos;
CREATE TABLE FABRICA.articulos(
    codigo_articulo VARCHAR2(20),
    fecha_registro_cliente DATE,
    nombre VARCHAR2(20) NOT NULL,
    precio NUMBER(2),
    fecha_pedido DATE);

REVOKE ALL ON COMERCIAL.pedidos FROM PUBLIC;
GRANT READ ON COMERCIAL.pedidos TO COMERCIAL;

REVOKE ALL ON COMERCIAL.clientes FROM FABRICA;

GRANT READ,INSERT,UPDATE,DELETE ON FABRICA.articulos TO COMERCIAL
    WITH GRANT OPTION;

CREATE USER JEFE_COMERCIAL
    IDENTIFIED BY JEFE_COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
GRANT ALL ON COMERCIAL.pedidos TO JEFE_COMERCIAL
    WITH GRANT OPTION;
GRANT ALL ON COMERCIAL.clientes TO JEFE_COMERCIAL
    WITH GRANT OPTION;

SELECT c.nombre, SUM(p.precio_total)
    FROM COMERCIAL.pedidos p
    JOIN COMERCIAL.clientes c ON p.cliente_codigo=c.codigo
    GROUP BY c.nombre;

-------------------------------------------------------------------------------------------------------------------------------------------------
ALTER SESSION SET "_ORACLE_SCRIPT" = TRUE;

CREATE TABLESPACE TBSP_COMERCIAL
    DATAFILE 'C:\ABD\TS\tbsp_comercial.dbf'
    SIZE 1M;
CREATE TABLESPACE TBSP_FABRICA
    DATAFILE 'C:\ABD\TS\tbsp_fabrica.dbf'
    SIZE 1M;

CREATE USER COMERCIAL IDENTIFIED BY COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
CREATE USER FABRICA IDENTIFIED BY FABRICA
    DEFAULT TABLESPACE TBSP_FABRICA;

DROP TABLE COMERCIAL.clientes;
CREATE TABLE COMERCIAL.clientes(
    codigo INT,
    nombre VARCHAR(50),
    fecha_registro DATE);

CREATE TABLESPACE TBSP_PEDIDOS
    DATAFILE 'C:\ABD\TS\tbsp_pedidos.dbf'
    SIZE 1M
    AUTOEXTEND ON MAXSIZE 10M;

DROP TABLE COMERCIAL.pedidos;
CREATE TABLE COMERCIAL.pedidos(
    num_pedido INT,
    cliente_codigo INT,
    articulo_codigo VARCHAR2(20),
    precio_total NUMBER,
    fecha_pedido DATE) TABLESPACE TBSP_PEDIDOS;

DROP TABLE FABRICA.articulos;
CREATE TABLE FABRICA.articulos(
    codigo_articulo VARCHAR2(20),
    fecha_registro_cliente DATE,
    nombre VARCHAR2(20) NOT NULL,
    precio NUMBER(2),
    fecha_pedido DATE);

REVOKE ALL ON COMERCIAL.pedidos FROM PUBLIC;
GRANT READ ON COMERCIAL.pedidos TO COMERCIAL;

REVOKE ALL ON COMERCIAL.clientes FROM FABRICA;

GRANT READ,INSERT,UPDATE,DELETE ON FABRICA.articulos TO COMERCIAL
    WITH GRANT OPTION;

CREATE USER JEFE_COMERCIAL
    IDENTIFIED BY JEFE_COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
GRANT ALL ON COMERCIAL.pedidos TO JEFE_COMERCIAL
    WITH GRANT OPTION;
GRANT ALL ON COMERCIAL.clientes TO JEFE_COMERCIAL
    WITH GRANT OPTION;

SELECT c.nombre, SUM(p.precio_total)
    FROM COMERCIAL.pedidos p
    JOIN COMERCIAL.clientes c ON p.cliente_codigo=c.codigo
    GROUP BY c.nombre;
































































