#  Past Simple

Past simple, también llamado simple past o past tense, es el pasado en inglés. Sirve para expresar acciones pasadas, tanto cercanas en el tiempo como lejanas. No es relevante la duración de la acción en sí, sino si la acción se ha completado o no.

### Uso
El past simple se emplea en inglés para expresar:

- acciones pasadas;

Ejemplo:

Last month a girl from China joined our class.
She showed us where she was from on a map.

- una sucesión de acciones en el pasado;

Ejemplo:

She came in, introduced herself, and began to talk about her country.

- la interrupción de un curso de acción;

Ejemplo:

While she was talking about her home town, the school bell suddenly rang.


- Affirmative: I went home one week ago.
- Negative: He didn't watch tv last month.
- Interrogative: Did you drink beer last sunday?.

# Past Continous

Past continuous, o past progressive, se utiliza en inglés para acciones que se encuentran en curso o en progresión en un momento determinado del pasado. Se forma con el verbo auxiliar be en pasado y el gerundio del verbo principal.

#### was were
#### +ing

- Affirmative: I was playing hockey for 2 hours yesterday.
- Negative: No, I wasn't studying
- Interrogative: Were you calling me when I was sleeping?.




# Past simple/Past Continous

I was playing video games when mi dad scolded me.

I was studying when my friends made me a video called.

**While** I was playing video games (eliminamos when) mi dad scolded me.


# Past Perfect Simple


- Affirmative: I had played rugby.

- Interrogative: Had you visited London?.

- Negative: She hadn't eaten anything.




































