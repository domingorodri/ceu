# Proyecto de redes

### DHCP

Debemos modificar el archivo de interfaces.


```
/etc/default/isc-dhcp-server
```

Tenemos que descomentar las dos lines de "DHCPDv4" y poner el nombre de nuestra interfaz.

![foto](1.png.jpg) 

<br>
<br>

## Modificamos el archivo de configuración de DHCP

Vamos rellenando los datos que necesitemos

![foto](2.jpg)

Vamos completando la configuracón con la puerta de enlace, broadcast, el rango de las direcciones ip, mascara, red.

![foto](3.jpg)

Para ver si funciona nuestro servicio debemos comprobarlo con el comando:

```
systemctl restart isc-dhcp-server

systemctl status isc-dhcp-server
```

### Direcciones IP Reserva

Debemos poner el nombre del host, su direccion MAC, y la dirección IP.

Una vez echo las reservas debemos reiniciar el servicio y en nuestros clientes de "linux" deberan poner:

```
sudo dhclient enp0s3
```
<br>

![foto](4.jpg)
