 # Servicios de Red e Internet
## Configuración Apache
### Fecha: 03/10/2022
**Tareas a realizar**:

1. Configura el dominio markdown.es en tu servidor de DNS local (Máquina virtual).


    ```
    sudo nano /etc/hosts
    ```
    

     Debemos colocar la direccion de nuestra máquina 

    ![/etc/hosts!](Fotos/PrtScr%20capture.jpg)
2. Configura el dominio markdown.es en tu equipo (DNS local) para que apunte a la IP de tu MV.
    
    Dentro de este directorio deberemos añadir la direccion de nuestra maquina y el nombre de nuestro sitio WEB, en mi caso markdown.es

    ```
    C:\Windows\System32\drivers\etc\hosts
    ```
    
    ![/etc/hosts!](Fotos/1.jpg)


3. Conéctate a tu MV a través de un cliente SSH (Ej.: Putty).

    Para conectar la maquina por ssh tenemos que hacer unos pasos previos que yo los tengo echos ya, aunque los comentaré.


    ```
    sudo apt install ssh
    ```
    
    Instalaremos el servicio de SSH y lo habilitaremos.
    

    ``` 
    sudo systemctl start ssh
    ```

    ```
    systemctl status ssh
    ```

    En mi caso yo utilizo MobaXterm como terminal SSH.
    Para conectarnos debemos colocar la direccion IP y tener habilitado el puerto 22 de la maquina ubuntu.

    ![/etc/hosts!](Fotos/PrtScr%20captur4e.jpg)

    Una vez colocamos la direccion IP nos pedira el login.

    Deberemos colocar la contraseña y ya estaremos dentro.

    ![/etc/hosts!](Fotos/PrtScr%20capture_2.jpg)

4. Crea un fichero de configuración en Apache (virtualhost) para markdown.es.
    
    Aqui debemos modificar el archivo de configuracion del sitio.
    En mi caso lo que he echo ha sido copiar uno que ya tenía echo y configurarlo segun mis necesidades.

    ```
    nano /etc/apache2/sites-enable/markdown.es.conf
    ```

    ![/etc/hosts!](Fotos/markdown.es.conf.jpg)

5. Activa el sitio markdown.es.

    Hay dos comandos para poder activar un sitio:
    
    ```
    a2ensite markdown.es --- a2ensite markdown.es.conf
    ```

    Me dara un fallo ya que he activado el sitio anteriormente.

    ![/etc/hosts!](Fotos/activacion.jpg)

    Una vez esté activo podremos acceder.

6. Modificaciones y consideraciones a tener en cuenta: la carpeta principal del sitio será /home/markdown.es/. Se quiere que no se listen los ficheros y carpetas cuando no se encuentre index.html. El fichero por defecto que debe cargarse al acceder a markdown.es debe ser inicio.html (crea el fichero si lo necesitas). El error 404 debe mostrar por pantalla un fichero html denominado error404.html con el mensaje "Recurso no encontrado".

    Debemos acceder al fichero de configuracion de apache.


    ```
    nano /etc/apache2/apache2.conf
    ```

    ![/etc/hosts!](Fotos/apachecon.conf.jpg)

    Debemos permitir el directorio /home/markdown.es y sus configuraciones pertinentes.

    ![/etc/hosts!](Fotos/configuracion6.jpg)

    Lo que debemos hacer sera:

    DirectoryIndex inicio.html (para que coja automaticamente el archivo inicio.html)
    
    ErrorDocument 404 /error.html (debemos crear el archivo error.html y colocar "Recurso no encontrado").

    ![/etc/hosts!](Fotos/prueba.jpg)



    ![/etc/hosts!](Fotos/inicio.jpg)

    Como podemos ver va directo a inicio.html
    
    ![/etc/hosts!](Fotos/error.jpg)

    Como podemos ver si vamos a otro recurso nos dira que no esta disponible.



    
    Fuente interesante para documentar en markdown: https://www.markdownguide.org/
