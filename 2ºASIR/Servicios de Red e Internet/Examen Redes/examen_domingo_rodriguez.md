# Servicios de Red e Internet

### Examen T7-8

Descomprimimos el archivo de openfire

```
tar -xvf openfire...
```
Instalaremos

```
sudo apt install openjdk-8-jre
```

Creamos un enlace simbolico

```
sudo ln -s /home/semanaverde/Escritorio/openfire/bin/openfire /etc/init.d
```
![foto](1.png)

Actualizamos y instalamos mariadb

```
sudo apt-get update
sudo apt-get install mariadb-server
sudo mysql_secure_installation
```


Comandos para crear la base de datos y usuario

```
sudo mysql -u root
create database ofbbdd character set='utf8';
create user 'ofbbddus'@'localhost' identified by 'laclavedeofbbddus';
grant all privileges on 'ofbbdd' .* to 'ofbbddus'@'localhost' with grant option;
flush privileges;
quit;
```


