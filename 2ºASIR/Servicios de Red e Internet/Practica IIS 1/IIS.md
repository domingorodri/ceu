# Servicios de Red e Internet

### Ejercico de clase IIS

Crea un nuevo sitio web para prueba.es con un inicio.html que se abra por defecto. El directorio debe ser /inetpub/wwwroot/prueba.es.

<br>



<br>

Lo primero que debemos hacer sera crear el directorio prueba.es dentro de /inetpub/wwwroot/

Debemos modificar el archivo "hosts"

```
C:\Windows\System32\drivers\etc\hosts
```

Debemos hacer que cuando busquemos "prueba.es" debe apuntar a nuestra dirección local para que lo busque en nuestra maquina.

127.0.0.1 -------------------- prueba.es


- Cuando agregamos el sitio Web debemos colocar los siguientes datos

    - Nombre del sitio: prueba.es
    - Ruta de acceso fisica: C:\inetpub\wwwroot\prueba.es
    - Direccion IP: Todas las no asignadas
    - Puerto 80 

![Fotos](Fotos/1.jpg)
 
<br>

En nuestra ruta de acceso debemos crear el archivo inicio.html para que cuando entremos a nuestra direccion "prueba.es" nos lleve directamente al archivo inicio.html asi que debemos habilitar que nos coja automaticamente dicho archivo.

<br>

Para que por defecto se nos abra dicho archivo debemos ir a la opción de "Documento predeterminado" y ahi escribir el nombre de nuestro archivo que estará en nuestro directorio que queremos que se abra al iniciar nuestra página.

![Fotos](Fotos/2.jpg)

<br>


Aquí la prueba de que nuestro sitio web funciona perfectamente.

![Fotos](Fotos/3.jpg)






