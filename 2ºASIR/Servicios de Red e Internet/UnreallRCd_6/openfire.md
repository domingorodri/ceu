# Servicios de Red e Internet

## Actividad servidor de chat

Nosotros haremos el servidor de chat UnreallRCd.

Para ello lo primero que haremos sera descargar la version estable que es la versión 6.0.4.2 de linux.

![foto](1.jpg)

Nos descargaremos el archivo tar y lo descomprimimos.

En los prerequisitos debemos instalar esto.

```
sudo apt-get install build-essential pkg-config libssl-dev libpcre2-dev libargon2-dev libsodium-dev libc-ares-dev libcurl4-openssl-dev
```
<br>

Debemos añadir un usuario para el IRCd.

```
sudo adduser unrealircd
```
Iniciaremos con dicho usuario para realizar todas las acciones.

<br>
Descargaremos el tar.

```
wget --trust-server-names https://www.unrealircd.org/downloads/unrealircd-latest.tar.gz
```

Para comenzar debemos estar en la carpeta del IRCd.

```
./Config
```

Nos pedirá los parametros para configurar el servidor de chat.
<br>

```
make
```

Para compilar el UnreallRCd.

<br>

Finalmente para instalarlo debemos colocar este comando.

```
make install
```
<br>

### Archivo de configuración

Cambiaremos al directorio instalado de UnrealIRCd, este es /home/usuario/unrealircd por defecto.

```
cd ~/unrealircd
```

Copiaremos un archivo de configuración y lo modificaremos a nuestro gusto.

```
cp conf/examples/example.conf conf/unrealircd.conf
```
<br>

Debemos correr el comando gen debemos generar los hashes.


Para iniciar el servidor de chat.

```
./unrealircd start
```


![foto](2.jpg)


Nosotros nos hemos descargado el Weechat y ya empezamos a chatear.

