
# Servicio de redes e internet

## Servidor Web NGINX
 
### Ejercicio 1
 
Enunciado:
- Conéctate a tu MV por SSH.
    ![Foto](Fotos/ssh.jpg)
- Instala NGINX en tu MV Ubuntu limpia.
    ```
    sudo apt get install nginx
    ```
- Utiliza el dominio minginx.es apuntando a tu máquina (127.0.0.1).
    
    ```
    nano /etc/hosts
    ```
    Debemos colocar la siguiente linea.
    
    127.0.0.1   minginx.es

- El directorio por defecto debe ser /var/www/minginx.es.

    ![Foto](Fotos/3.jpg)
- Crea un fichero de configuración para minginx.es y desactiva (no eliminar solo desactivar) el sitio por defecto (default) y otros que haya activos.

    ![Foto](Fotos/2.jpg)
    ![Foto](Fotos/1.jpg)
- El fichero que cargará por defecto es inicio.html que mostrará por defecto un mensaje de saludo "Bienvenido". Debes crear ese archivo inicio.html y hacer que sea por defecto al acceder al dominio minginx.es.

    ![Foto](Fotos/4.jpg)

- Ahora cambia el puerto de escucha por el 8005.
    
    ![Foto](Fotos/5.jpg)
- Activa el sitio minginx.es y comprueba que se accede correctamente a dicho puerto y que carga inicio.html.
    ```
    ln -s /etc/nginx/sites-available/minginx.es ../sites-enabled/
    ```
- Instala php-fpm y pruébalo creando un archivo llamado info.php a /var/www/minginx.es con el siguiente código:
    
    ![Foto](Fotos/6.jpg)

    ```
    <?php echo "Es correcto."; ?>
    ```

    Debemos ver la versión de php con "php -v" y es la que deberemos colocar en la opcion de location. 

    ![Foto](Fotos/7.jpg)