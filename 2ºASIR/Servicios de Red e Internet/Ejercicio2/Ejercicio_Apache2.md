# Servicio de Redes e internet.

## Ejercicio Apache

**Actividad 1**

- Utiliza el dominio asir2.es apuntando a tu máquina y con la siguiente configuración:

    Debemos configurar el siguiente archivo para decirle que cuando se busque "asir2.es" donde debe mirar primero es en nuestro localhost (127.0.0.1).
    ```
    nano /etc/hosts
    ```
    ![foto1](Fotos/1.png)

- El directorio por defecto debe ser /var/www/asir2.es

    Debemos configurar la opción de DocumenRoot para que nuestro directorio por defecto sea la dirección que nos indica el ejercicio.

    ![foto1](Fotos/01.png)

- Como inicialmente no existe index.html, debe mostrarse error 403 Forbidden (no mostrar el listado de carpetas y archivos).
- El fichero que cargará por defecto es esinicio.html que mostrará por defecto un mensaje "Empezamos".

    Para que el primer archivo en cargar sea el de "esinicio.html" debemos irnos a la opción de DirectoryIndex y colocar el archivo que quieres que nos coja como inicio.

    ![foto1](Fotos/2.png)

- Si se produce un error 403 debe mostrar un archivo error403.html que muestre "No se puede acceder".

    Como ya he creado los directorios y no tienen permisos nos debe salir el error 403, que nos redireccionará al archivo de error403.html que creamos anteriormente.

    ![foto1](Fotos/3.png)
- Crea una carpeta "demo" dentro de /var/www/asir2.es. Esta carpeta no debe tener permisos de acceso.
- Crea una subcarpeta "otro" dentro de /var/www/asir2.es/demo. Esta carpeta debe tener permisos de acceso.

    ![foto1](Fotos/4.png)

- El puerto de escucha debe ser el 8031.


    Lo he cambiado en dos sitios.

    ```
    nano /etc/apache2/ports.conf
    ```
    ![foto1](Fotos/5.png)

    En nuestro fichero de configuracion del sitio también debemos cambiar el puerto.

    ![foto1](Fotos/6.png)

- Cuando se entre en asir2.es:8031/enlace/ se debe cargar un archivo enlace.html que debe estar alojado en /home/asir2.es. Emplea Alias.

    Como tengo creado un enlace lo he aprobechado para hacer la prueba.

    ![foto1](Fotos/7.png)

- Crea un directorio protegido en /var/www/asir2.es que se llamará "admin" y que solo podrá ser accesible mediante Auth Basic con las siguientes credenciales:
    - Usuario: admin
    - Clave: laClave-2020

    
    htpasswd -c htpass admin

    Debemos colocar la contraseña que se nos indica 

    ![foto1](Fotos/ht.png)
