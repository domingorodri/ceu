# Actividad de clase 2 (IIS)



Utiliza el dominio asir.com apuntando a tu máquina local.

Debemos ir al archivo hosts del sistema y configuramos para que la dirección "asir.com" apunte a nuestra máquina.

```
C:\Windows\System32\drivers\etc\hosts
```
<br>

![foto](Fotos/1.jpg)

<br>

Emplea como directorio C:\asir.com\

 Debemos crear el directorio de asir.com y le daremos permisos a la carpeta para poder modificar los archivos de dentro.

![foto](Fotos/2.jpg)

<br>

Cambia el puerto de escucha por el 8081.

Cuando he empezado a crear el sitio me ha dado la opción de cambiar el puerto, he cambiado el puerto por defecto que es el puerto 80 por el 8081.

![foto](Fotos/3.jpg)

<br>

Crea un fichero lapaginadeinicio.html con el texto "Esto carga al empezar" en el directorio por defecto.

Una vez creado el archivo HTML, lo editaremos y pondremos lo que nos pide el ejercicio "Esto carga al empezar" y nos iremos a modificar el archivo predeterminado. Debemos escribir el nombre del archivo que queremos que salga por defecto al entrar en nuestra página.

![foto](Fotos/4.jpg)


Vemos como ponemos el archivo en documento predeterminado.

![foto](Fotos/5.jpg)


Aquí la prueba de que funciona.

![foto](Fotos/6.jpg)

<br>

Crea un enlace simbólico cuya ruta real sea c:\enlacesim y que se cargue al acceder a la url http://asir.com/enlace.

Veremos que en los directorios virtuales he creado como un "alias" para que me direccione a un directorio a través de un nombre.


![foto](Fotos/7.jpg)


Prueba
![foto](Fotos/8.jpg)