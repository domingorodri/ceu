--CUANTO SE HA GASTADO CADA CLIENTE

SELECT c.nombre_cliente, SUM(dp.cantidad*dp.precio_unidad)GASTADO FROM cliente C, PEDIDO P, detalle_pedido DP WHERE c.codigo_cliente = p.codigo_cliente
AND p.codigo_pedido = dp.codigo_pedido
GROUP BY c.nombre_cliente;

--CLIENTES QUE SE HAYAN GASTADO MAS DE > 10000�

SELECT c.nombre_cliente, SUM(dp.cantidad*dp.precio_unidad)AS GASTADO FROM cliente C, PEDIDO P, detalle_pedido DP WHERE c.codigo_cliente = p.codigo_cliente
AND p.codigo_pedido = dp.codigo_pedido
GROUP BY c.nombre_cliente
HAVING SUM(dp.cantidad*dp.precio_unidad) >= 10000
ORDER BY 2 DESC;

select * from (SELECT c.nombre_cliente, SUM(dp.cantidad*dp.precio_unidad)AS GASTADO FROM cliente C, PEDIDO P, detalle_pedido DP WHERE c.codigo_cliente = p.codigo_cliente
AND p.codigo_pedido = dp.codigo_pedido
GROUP BY c.nombre_cliente
HAVING SUM(dp.cantidad*dp.precio_unidad) >= 10000
ORDER BY 2 DESC) where GASTADO = (select max(GASTADO) from (SELECT c.nombre_cliente, SUM(dp.cantidad*dp.precio_unidad)AS GASTADO FROM cliente C, PEDIDO P, detalle_pedido DP WHERE c.codigo_cliente = p.codigo_cliente
AND p.codigo_pedido = dp.codigo_pedido
GROUP BY c.nombre_cliente
HAVING SUM(dp.cantidad*dp.precio_unidad) >= 10000
ORDER BY 2 DESC));