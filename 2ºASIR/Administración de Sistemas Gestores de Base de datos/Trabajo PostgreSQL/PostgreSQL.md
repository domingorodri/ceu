# **Instalacion de PostgreSQL**
## ¿Que es y para que se utiliza?
PostgreSQL es un sistema de codigo abierto de administracion de base de datos del tipo relacional, aunque tambien es posible ejecutar consultas que sean no relaciones. En este sistema, las consultas relacionales se basan en SQL, mientras que las no relacionales hacen uso de JSON.

## Características
1. **Es de código abierto**
    
     PostgreSQL se ha vuelto tan popular porque se trata de un sistema de código abierto y esto ha permitido que la comunidad pueda mejorarlo.

2. **Es gratuito**
    
     Se trata de un sistema totalmente gratis por lo que no hay que pagar nada para utilizarlo.

3. **Multiplataforma**

    Es un software que puede correr en diversos sistemas operativos

4. **Puede manejar gran volumen de datos**

    Una de sus capacidades mas importantes es su gran manejo de volumenes de datos, cosa que otros sistemas como MySQL no lo hace tan bien.

## Instalación
1. Debemos actualizar los repositorios y paquetes del sistema:
    ```
    sudo apt-get update  ----Actualizar información de los repositorios.

    sudo apt-get upgrade ----Instalar parches y nuvas versiones.
    ``` 
2. Debemos instalar el SSH en nuestro servidor con el comando.
    
    ```  
    sudo apt install ssh
    ```
    Con este comando instalaremos el servicio de SSH para conectarnos a la maquina por remoto.

3. Debemos conectarnos por SSH desde nuestra aplicacion de preferencia, como pueden ser "Putty,  mobaxterm, winscp, etc.."
    
    ![Foto SSH](Fotos/1.jpg)

    
    En la imagen vemos como con la aplicación de MobaXterm me conecto por SSH a nuestro servidor Ubuntu.

4. Instalaremos el gestor de base de datos de PostgreSQL.
    
    Para la instalacion deberemos colocar este comando.

    ```
    sudo apt install postgresql postgresql-contrib -y -----> Instalacion de PostgreSQL.
    systemctl status PostgreSQL ---------------------------> Ver el status del servicio.
    systemctl start PostgreSQL ----------------------------> Inicializar el servicio.
    systemctl stop PostgreSQL -----------------------------> Para el servicio servicio.
    ```
    ![Foto SSH](Fotos/2.jpg)

    Dentro de la configuracion de postgre deberemos tocar dos archivos.

    ```
    nano /etc/postgresql/14/main/postgresql.conf

    nano /etc/postgresql/14/main/pg_hba.conf
    ```

    Este es el fichero de postgresql.conf donde debemos poner que escuche por una dirección ip, en mi caso con la opción (*) escuchará a cualquier dirección ip. 
    ![Foto SSH](Fotos/3.jpg)

    En el siguiente archivo debemos cambiar lo que tengo señalado en rojo por cualquier direccion ip que se pone con la opcion (0.0.0.0/0).

    ![Foto SSH](Fotos/4.jpg)


    Debemos hacer un "systemctl restart PostgreSQL" para restablecer el servicio.


    Debemos habilitar el firewall y dejar solo los puertos que utilizamos para que sea una conexion segura.

    Con el comando "netstat -pnltu" veremos los puertos que tenemos abiertos y el programa que lo esta utilizando.

    ![Foto SSH](Fotos/5.jpg)


    Como podemos ver ya podemos conectarnos por remoto a traves de la aplicacion de DBeaver a nuestra base de datos de PostgreSQL.

    ![Foto SSH](Fotos/6.jpg)

    

    





































