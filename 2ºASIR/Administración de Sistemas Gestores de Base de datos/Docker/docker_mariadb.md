# Administración de Sistemas Gestores de Base de Datos


### Manual Instalación de una base de datos MariaDB sobre Debian en un Docker


  - Instalar una máquina virtual con CentOS, sobre ello, instalar un docker de debian, instalar posteriormente MariaDB y hacer que sea accesible desde fuera de la maquina anfitriona.


Empezaremos por descargar la imagen del sistema CentOS o si tenemos instalada la aplicacion de vagrant podemos hacer:

```Zǎo shang hǎo zhōng guó! Xiàn zài wǒ yǒu bing chilling Wǒ hěn xǐ huān bing chilling Dàn shì "sù dù yǔ jī qíng jiǔ" bǐ bing chilling "sù dù yǔ jī qíng, sù dù yǔ jī qíng jiǔ" Wǒ zuì xǐ huān Suǒ yǐ xiàn zài shì yīn yuè shí jiān Zhǔn bèi Yī, èr, sān
vagrant init
```

![Foto](Fotos/1.jpg)

Esto hará que nos manden al directorio donde estamos haciendo  el "vagrant init" un vagrantfile donde haremos lo siguiente:

En nuestro vagrantfile donde pone 

```
config.vm.box = "base"
```

Debemos colocar la base que queramos, en mi caso debo colocar "centos/7"

![Foto](Fotos/2.jpg)


Lanzamos la maquina con vagrant up y cuando accedemos a la maquina yo en mi caso instalaré ssh para conectarme.

```
sudo yum install ssh
```

Tambien le he asignado a la interfaz enp0s3 un dhcp


```
sudo dhclient enp0s3
```

### Instalaremos docker dentro de nuestro CentOs

#### Todos los comandos se deben utilizar con un superusuario

Antes de instalar docker tenemos que instalar una serie de paquetes

```
yum install -y yum-utils device-mapper-persistent-data lvm2
```


Debemos añadir el repositorio oficial de Docker

```
yum-config-manager  --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

Instalaremos docker dentro de nuestro CentOs

```
yum install docker-ce
```

Lo habilitaremos

```
systemctl enable docker
```
Lo iniciaremos

```
systemctl start docker
```

### Uso de Docker

Para ver las imagenes --------------- > docker images

![Foto](Fotos/3.jpg)

Para ver contenedores activos ------- > docker ps

![Foto](Fotos/4.jpg)

Para ver contenedores no activos ---- > docker ps -a

![Foto](Fotos/5.jpg)

Buscaremos una imagen

```
docker search [imagen]

docker search debian
```


Descargaremos una imagen

```
docker pull [imagen]

docker pull debian
```

Para iniciar un contenedor debemos

```
docker run [imagen]

docker run debian
```

Cuando lanzamos un contenedor y entramos, cuando salgamos si queremos que no se "duerma" deberemos salir pulsando "CTRL+P+Q", esto hara que no nos apague el contenedor.

Para lanzarlo

![Foto](Fotos/7.jpg)



### Instalación de MariaDB

Instalamos paquetes necesarios

```
apt-get install software-properties-common dirmngr gnupg2 -y

apt-get install apt-transport-https wget curl -y
```

Instalaremos MariaDB

```
apt-get install mariadb-server -y
```

![Foto](Fotos/6.jpg)

Lo iniciaremos 

```
/etc/init.d/mariadb start
```








































