create table dept_copia as select * from dept;


drop table emp_copia;
create table emp_copia as select * from emp;
--create table emp_copia_vacio as select * from emp where 1=2;

select * from emp_copia;

 
CREATE OR REPLACE TRIGGER NO_CAMBIA_DEPT
BEFORE --AFTER
UPDATE OF DEPTNO ON emp_copia --ACCIONES
FOR EACH ROW -- PUEDE QUE NO ESTE---PERO SI ESTA EXISTEN LAS VARIABLES :NEW Y :OLD
/*DECLARE
supervisa INTEGER;*/
BEGIN
    IF :NEW.DEPTNO <> :OLD.DEPTNO THEN 
        raise_application_error(-20669,'HAY UN NOTA QUE SE QUIERE CAMBIAR');
    END IF;
END; 

SELECT * FROM SYS.all_triggers;

UPDATE emp_copia
SET DEPTNO = 10
WHERE ENAME = 'JAMES';

SELECT * FROM EMP_COPIA;
------------------------------------------------------------------------------------
create table emp_copia1 as select * from emp;


CREATE OR REPLACE TRIGGER COPIAR
/*BEFORE*/ AFTER
UPDATE OR INSERT OR DELETE ON emp_copia1 --ACCIONES
FOR EACH ROW -- PUEDE QUE NO ESTE---PERO SI ESTA EXISTEN LAS VARIABLES :NEW Y :OLD
/*DECLARE
supervisa INTEGER;*/
BEGIN
    IF DELETING THEN 
        DELETE emp_copia2
        WHERE EMPNO = OLD.EMPNO;
        END IF;
    IF INSERTING THEN 
        INSERT INTO emp_copia2
        SELECT * FROM emp_copia1 WHERE EMPNO=:NEW.EMPNO;
        END IF;
    IF UPDATING THEN 
        UPDATE emp_copia2
        set ENAME = :NEW.ENAME,
        SAL = :NEW.SAL,
        EMPNO = :NEW.EMPNO
        WHERE EMPNO = :OLD.EMPNO;
        END IF;     
END;
--------------------------------------------------------------------------------------------

-- Ejercicio 1
-- Haz un trigger que solo permita a los vendedores tener comisiones.

CREATE OR REPLACE TRIGGER NO_COMM_NO_SALES
BEFORE --AFTER
INSERT OR UPDATE OF COMM OR UPDATE OF JOB ON emp_copia --ACCIONES
FOR EACH ROW
BEGIN
        IF UPPER(:NEW.JOB) <> 'SALESMAN' AND :NEW.COMM IS NOT NULL THEN
                raise_application_error(-20669,'NO SE PERMITEN COMISION A ALGUIEN QUE NO SEA VENDEDOR');
        END IF;
END; 

----------------------------------------------------------------------------

-- Ejercicio 3
-- Haz un trigger que controle si los sueldos est�n en los siguientes rangos:
-- CLERK: 800 � 1100
-- ANALYST: 1200 � 1600
-- MANAGER:1800 � 2000

CREATE OR REPLACE TRIGGER sal_range
    BEFORE INSERT OR UPDATE
    ON EMP_COPIA
    FOR EACH ROW
BEGIN
    IF UPPER(:NEW.JOB) = 'CLERK' AND :NEW.SAL NOT BETWEEN 800 AND 1100

 

    THEN
        raise_application_error(-20669, 'Clerk salary must be between 800-1100');
    ELSIF UPPER(:NEW.JOB) = 'ANALYST' AND :NEW.SAL NOT BETWEEN 1200 AND 1600
    THEN
        raise_application_error(-20669, 'Analyst salary must be between 1200-1600');
    ELSIF UPPER(:NEW.JOB) = 'MANAGER' AND :NEW.SAL NOT BETWEEN 1800 AND 2000
    THEN
        raise_application_error(-20669, 'Manager salary must be between 1800-2000');
    END IF;
END;
/
UPDATE EMP_COPIA SET SAL=5 WHERE EMPNO=7788;

-----------------------------------------------------------------------------------------

-- Ejercicio 4
-- Haz un trigger que impida al usuario MANOLO que cambie el sueldo de los empleados que trabajan en DALLAS.
 
CREATE OR REPLACE TRIGGER SUELDO_DALLAS
BEFORE --AFTER
UPDATE OF SAL ON EMP --ACCIONES
FOR EACH ROW -- PUEDE QUE NO ESTE---PERO SI ESTA EXISTEN LAS VARIABLES :NEW Y :OLD
DECLARE
V_LOC EMP.LOC%TYPE;
BEGIN

    IF USER = 'MANOLO' THEN
    SELECT UPPER(LOC) INTO V_LOC
    FROM DEPT
    WHERE DEPTNO = (:NEW.DEPTNO);
END; 

-----------------------------------------------------------------------

--Impedir que se pidan productos que no tiene stock suficiente.

CREATE OR REPLACE TRIGGER NO_PEDIR_SIN_STOCK
BEFORE
INSERT OR UPDATE OF CANTIDAD OR UPDATE OF CODIGOPRODUCTO ON DETALLEPEDIDOS
FOR EACH ROW
DECLARE
    V_CANT_STOCK productos.cantidadenstock%TYPE;
BEGIN
    SELECT cantidadenstock INTO V_CANT_STOCK
    FROM PRODUCTOS
    WHERE codigoproducto = :NEW.CODIGOPRODUCTO;
    
    IF V_CANT_STOCK < :NEW.CANTIDAD THEN
        raise_application_error(-20669,'NO HAY STOCK SUFICIENTE');  
    END IF;
END;

-----------------------------------------------------------------------------

-- Poner comentario con retraso en pedidos

CREATE OR REPLACE TRIGGER PONER_COMENTARIO
BEFORE 
INSERT OR UPDATE OF FECHAESPERADA OR UPDATE OF FECHAENTREGA ON PEDIDOS
FOR EACH ROW
BEGIN
    IF :NEW.FECHAENTREGA > :NEW.FECHAESPERADA THEN
        :NEW.COMENTARIOS := 'PEDIDO CON RETRASO';
    END IF;
    
END;











