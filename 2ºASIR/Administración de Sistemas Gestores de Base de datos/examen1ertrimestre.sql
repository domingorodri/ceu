ALTER SESSION SET "_ORACLE_SCRIPT" = TRUE;
------------------------------------------------
CREATE TABLESPACE TBSP_COMERCIAL
    DATAFILE 'C:\ABD\TS\tbsp_comercial.dbf'
    SIZE 1M;
------------------------------------------------
CREATE TABLESPACE TBSP_FABRICA
    DATAFILE 'C:\ABD\TS\tbsp_fabrica.dbf'
    SIZE 1M;
------------------------------------------------
CREATE USER COMERCIAL IDENTIFIED BY COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
------------------------------------------------
CREATE USER FABRICA IDENTIFIED BY FABRICA
    DEFAULT TABLESPACE TBSP_FABRICA;
------------------------------------------------
DROP TABLE COMERCIAL.clientes;
------------------------------------------------
CREATE TABLE COMERCIAL.clientes(
    codigo INT,
    nombre VARCHAR(50),
    fecha_registro DATE);
------------------------------------------------
CREATE TABLESPACE TBSP_PEDIDOS
    DATAFILE 'C:\ABD\TS\tbsp_pedidos.dbf'
    SIZE 1M
    AUTOEXTEND ON MAXSIZE 10M;
------------------------------------------------
DROP TABLE COMERCIAL.pedidos;
------------------------------------------------
CREATE TABLE COMERCIAL.pedidos(
    num_pedido INT,
    cliente_codigo INT,
    articulo_codigo VARCHAR2(20),
    precio_total NUMBER,
    fecha_pedido DATE) TABLESPACE TBSP_PEDIDOS;
---------------------------------------------------------------
DROP TABLE FABRICA.articulos;
---------------------------------------------------------------
CREATE TABLE FABRICA.articulos(
    codigo_articulo VARCHAR2(20),
    fecha_registro_cliente DATE,
    nombre VARCHAR2(20) NOT NULL,
    precio NUMBER(2),
    fecha_pedido DATE);
---------------------------------------------------------------
REVOKE ALL ON COMERCIAL.pedidos FROM PUBLIC;
---------------------------------------------------------------
GRANT READ ON COMERCIAL.pedidos TO COMERCIAL;
---------------------------------------------------------------
REVOKE ALL ON COMERCIAL.clientes FROM FABRICA;
---------------------------------------------------------------
GRANT READ,INSERT,UPDATE,DELETE ON FABRICA.articulos TO COMERCIAL
    WITH GRANT OPTION;
--------------------------------------------------------------
CREATE USER JEFE_COMERCIAL
    IDENTIFIED BY JEFE_COMERCIAL
    DEFAULT TABLESPACE TBSP_COMERCIAL;
--------------------------------------------------------------
GRANT ALL ON COMERCIAL.pedidos TO JEFE_COMERCIAL
    WITH GRANT OPTION;
---------------------------------------------------------------
GRANT ALL ON COMERCIAL.clientes TO JEFE_COMERCIAL
    WITH GRANT OPTION;
---------------------------------------------------
SELECT c.nombre, SUM(p.precio_total)
    FROM COMERCIAL.pedidos p
    JOIN COMERCIAL.clientes c ON p.cliente_codigo=c.codigo
    GROUP BY c.nombre;
